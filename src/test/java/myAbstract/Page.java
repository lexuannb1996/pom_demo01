package myAbstract;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import myAbstract.BasePage;

public abstract class Page {
//chứa các phương thức trừu tượng được dùng lặp đi lặp lại
	WebDriver driver;// khai báo webdriver dùng chung lặp lại nhiều lần
	WebDriverWait wait;// timeout của webdriver, bằng thread.sleep
	// constructor: khi gọi đến nó sẽ được khởi tạo đầu tiên
	
		public Page(WebDriver driver) {
			this.driver = driver;
			this.wait = new WebDriverWait(this.driver, 15);
		}

		public abstract WebElement getElement(By locator);

		public abstract void waitForElementPresent(By locator);

		public abstract void waitForPageLoad();
		
		public abstract String getTitle();// dùng khi sử dụng verify title

		/*
		 * public Object getInstance(Object t) { 
		 * return t; 
		 * }
		 */
		public <TPage extends BasePage> TPage getInstance(Class<TPage> pageClass) {
			TPage T = null;
			try {
				
				//getDeclaredConstructor (khởi tạo cái pageClass) với biến truyền vào là driver
				//TPage tPage = new TPage(WebDriver);
				T = pageClass.getDeclaredConstructor(WebDriver.class).newInstance(this.driver);
				return  T;
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return T;
		}
	
}
