package myAbstract;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class BasePage extends Page{
	public BasePage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@Override
	public WebElement getElement(By locator) {
		WebElement elm = null;

		try {
			elm = driver.findElement(locator);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return elm;
	}

	@Override
	public void waitForElementPresent(By locator) {
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(locator));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void waitForPageLoad() {
		
		
	}

	@Override
	public String getTitle() {
		String titlle = "";
		try {
			titlle = driver.getTitle();
			return titlle;
		}
		catch(Exception e) {
			System.out.println("Cannot get Title from this page");
			e.printStackTrace();
		}
		return titlle;
	}
	

}
