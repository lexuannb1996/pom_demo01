package myTest;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import myAbstract.BasePage;
import myAbstract.Page;
import myPages.HomePage;
import myPages.LogInPage;
import myPages.RegisterPage;

public class SecurityTest extends BasePage{
	private RegisterPage res = null;
	String strUserID;
	String strPassword;
	@SuppressWarnings("null")
	@Test(priority = 1)
	public void verifyInfoRegistrationDisplayed() {
		Page page = null;
		res = page.getInstance(RegisterPage.class);
		res.registerUser();

		strUserID = res.getStrUsername();
		strPassword = res.getStrPassword();
		Assert.assertTrue(strUserID.length() > 0, "Verify User ID is displayed");
		Assert.assertTrue(strPassword.length() > 0, "Verify Password is displayed");
	}

	@Test(priority = 2)
	public void doLogin() {
		LogInPage loginPage = res.goToSecurityProject();
		HomePage homePage = loginPage.doLogin(strUserID, strPassword);
		
		String title = homePage.getTitleHomePage();
		Assert.assertEquals(title, "GTPL Bank Manager HomePage", "Verify Home Page opened");
	}
	public SecurityTest(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

}
