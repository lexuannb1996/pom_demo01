package myPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import myPages.LogInPage;

import myAbstract.BasePage;

public class RegisterPage extends BasePage{
	private By txtRegisterEmail_loc = By.name("emailid");
	private By btnSubmit_loc = By.name("btnLogin");
	private By txtUsername_loc = By.xpath("//td[contains(text(),\"User ID\")]/following-sibling::td");
	private By txtPassword_loc = By.xpath("//td[contains(text(),\"Password\")]/following-sibling::td");
	private By btnBankProject_loc = By.cssSelector("a[href*=\"http://demo.guru99.com/V1/index.php\"]");
	private By btnSecurityProject_loc = By.xpath("//a[contains(text(), \"Security Project\")]");
	private String strUsername;
	private String strPassword;

	public RegisterPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	public WebElement getTxtRegisterEmail() {		
		return getElement(txtRegisterEmail_loc);
	}
	public WebElement getBtnSubmit() {		
		return getElement(btnSubmit_loc);
	}
	public WebElement getTxtUsername_loc() {		
		return getElement(txtUsername_loc);
	}
	public WebElement getTxtPassword() {		
		return getElement(txtPassword_loc);
	}
	public WebElement getbtnBankProject() {		
		return getElement(btnBankProject_loc);
	}
	public WebElement getbtnSecurityProject() {		
		return getElement(btnSecurityProject_loc);
	}
	public void setStrUsername(String username) {
		this.strUsername = username;
	}
	
	public void setStrPassword(String password) {
		this.strPassword = password;
	}
	
	public String getStrUsername() {
		return this.strUsername;
	}
	
	public String getStrPassword() {
		return this.strPassword;
	}
	
	public String registerUser() {
		getTxtRegisterEmail().sendKeys("abc@gmail.com");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		getBtnSubmit().click();
		waitForElementPresent(txtUsername_loc);
		waitForElementPresent(txtPassword_loc);
		setStrUsername(getElement(txtUsername_loc).getText());
		System.out.println("Res - txtUsername_loc: " + getElement(txtUsername_loc).getText());
		System.out.println("Res - getStrUsername: " + getStrUsername());
		setStrPassword(getElement(txtPassword_loc).getText());
		System.out.println("Res - txtPassword_loc: " + getElement(txtPassword_loc).getText());
		System.out.println("Res - getStrPassword: " + getStrPassword());
		return getElement(txtPassword_loc).getText();
	}
	
//	public LogInPage goToBankProject() {
//		waitForElementPresent(btnBankProject_loc);
//		getbtnBankProject().click();
//		
//		//create the instance for LoginPage
//		return getInstance(LogInPage.class);
//	}
	public LogInPage goToSecurityProject() {
		waitForElementPresent(btnSecurityProject_loc);
		getbtnSecurityProject().click();
		
		//create the instance for LoginPage
		return getInstance(LogInPage.class);
	}
	
}
