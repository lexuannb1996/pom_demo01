package myPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


import myAbstract.BasePage;

public class LogInPage extends BasePage{

	private By txtUsername_loc = By.cssSelector("input[name=\"uid\"]");
	private By txtPassword_loc = By.cssSelector("input[name=\"password\"]");
	private By btnLogin_loc = By.cssSelector("input[name=\"btnLogin\"]");
	
	public LogInPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	public WebElement getTxtUsername() {
		return getElement(txtUsername_loc);
	}
	
	public WebElement getTxtPassword() {
		return getElement(txtPassword_loc);
	}
	
	public WebElement getTxtLogin() {
		return getElement(btnLogin_loc);
	}
	
	public void waitFortxtUsernamePresent() {
		waitForElementPresent(txtUsername_loc);
	}
	
	public void waitFortxtPasswordPresent() {
		waitForElementPresent(txtPassword_loc);
	}
	
	public void waitForBtnLoginresent() {
		waitForElementPresent(btnLogin_loc);
	}
	
	public HomePage doLogin(String userID, String password) {
		
		waitFortxtUsernamePresent();
		getTxtUsername().sendKeys(userID);
		waitFortxtPasswordPresent();
		getTxtPassword().sendKeys(password);
		waitForBtnLoginresent();
		getTxtLogin().click();
		
		return getInstance(HomePage.class);
	}

}
